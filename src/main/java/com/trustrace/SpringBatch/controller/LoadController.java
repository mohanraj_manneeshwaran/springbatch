package com.trustrace.SpringBatch.controller;

import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
public class LoadController {

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    @Qualifier("CsvToDb")
    Job CsvToDb;

    @Autowired
    @Qualifier("DbToDisplay")
    Job DbToDisplay;

    @GetMapping("/load")
    public BatchStatus load() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {


        Map<String, JobParameter> maps = new HashMap<>();
        maps.put("time", new JobParameter(System.currentTimeMillis()));
        JobParameters parameters = new JobParameters(maps);
        JobExecution jobExecution = jobLauncher.run(CsvToDb, parameters);

        System.out.println("JobExecution: " + jobExecution.getStatus());
        System.out.println("Job Id: "+jobExecution.getJobId());
        System.out.println("Job Instance: "+jobExecution.getJobInstance());
        System.out.println(jobExecution.isStopping());
        System.out.println("End time: "+jobExecution.getEndTime());
        System.out.println("Batch is Running");
        while (jobExecution.isRunning()) {
            System.out.println("...");
        }

        return jobExecution.getStatus();
    }


    @GetMapping("/download")
    public ResponseEntity<?> dowmload() throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException, IOException {


        Map<String, JobParameter> maps = new HashMap<>();
        maps.put("time", new JobParameter(System.currentTimeMillis()));
        JobParameters parameters = new JobParameters(maps);
        JobExecution jobExecution = jobLauncher.run(DbToDisplay, parameters);

        System.out.println("JobExecution: " + jobExecution.getStatus());
        System.out.println("Job Id: "+jobExecution.getJobId());
        System.out.println("Job Instance: "+jobExecution.getJobInstance());
        System.out.println(jobExecution.isStopping());
        System.out.println("End time: "+jobExecution.getEndTime());

        System.out.println("Batch is Running");
        while (jobExecution.isRunning()) {
            System.out.println("...");
        }

//        return ResponseEntity.ok().body("sss");

//        String filename = "src/main/resources/output.csv";
//
//        InputStreamResource file = new InputStreamResource((new FileInputStream(filename)));
//
//        return ResponseEntity.ok()
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=output.zip" )
//                .contentType(MediaType.parseMediaType("application/zip"))
//                .body(file);

        String filePath = "src/main/resources/output.xlsx";

        try {
            File file = new File(filePath);
           String zipFileName = file.getName().replace("xlsx","zip");

            FileOutputStream fos = new FileOutputStream(zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);

            zos.putNextEntry(new ZipEntry(file.getName()));

            byte[] bytes = Files.readAllBytes(Paths.get(filePath));
            zos.write(bytes, 0, bytes.length);
            zos.closeEntry();
            zos.close();

        } catch (FileNotFoundException ex) {
            System.err.format("The file %s does not exist", filePath);
        } catch (IOException ex) {
            System.err.println("I/O error: " + ex);
        }

        String fileName = "output.zip";

        InputStreamResource file = new InputStreamResource((new FileInputStream(fileName)));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=output.zip")
                .contentType(MediaType.parseMediaType("application/zip")).body(file);
    }
}

