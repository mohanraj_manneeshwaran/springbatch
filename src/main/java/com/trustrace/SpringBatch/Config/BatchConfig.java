package com.trustrace.SpringBatch.Config;

import com.trustrace.SpringBatch.DbToDisplay.CustomItemReader;
import com.trustrace.SpringBatch.model.DisplayResultDto;
import com.trustrace.SpringBatch.model.User;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.IOException;


@Configuration
@EnableBatchProcessing
public class BatchConfig {

    @Autowired
    @Qualifier("CsvToDbReader")
    ItemReader<User> itemReader;

    @Autowired
    @Qualifier("CsvToDbProcessor")
    ItemProcessor<User, User> itemProcessor;

    @Autowired
    @Qualifier("CsvToDbWriter")
    ItemWriter<User> itemWriter;

    @Bean("CsvToDb")
    public Job CsvToDb(JobBuilderFactory jobBuilderFactory,
                   StepBuilderFactory stepBuilderFactory){

        Step step = stepBuilderFactory
                .get("Step to store csv to db")
                .<User,User>chunk(600)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();

        return jobBuilderFactory
                .get("Job to store csv to db")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();

    }

    @Bean("CsvToDbReader")
    public FlatFileItemReader<User> itemReader() {


        System.out.println("---------------------From CsvToDbReader-------");
        FlatFileItemReader<User> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(new FileSystemResource("src/main/resources/salesRecords(1000).csv"));
        flatFileItemReader.setName("CSV-Reader");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setLineMapper(lineMapper());
        System.out.println("-------flatFileItemReader---------"+flatFileItemReader);
        return flatFileItemReader;
    }

    public LineMapper<User> lineMapper() {

        DefaultLineMapper<User> defaultLineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("region","country","itemType","salesChannel",
                                "orderPriority","orderDate","orderID","shipDate","unitsSold",
                                "unitPrice","unitCost","totalRevenue","totalCost","totalProfit");

        BeanWrapperFieldSetMapper<User> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(User.class);

        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);

        return defaultLineMapper;
    }


//    ---------------------------Download--------------------------
    @Autowired
    private MongoTemplate template;

    @Autowired
    @Qualifier("DbToDisplayReader")
    ItemReader<User> reader;

    @Autowired
    @Qualifier("DbToDisplayProcessor")
    ItemProcessor<User, DisplayResultDto> processor;   /// dto

    @Autowired
    @Qualifier("DbToDisplayWriter")
    ItemWriter<DisplayResultDto> writer;

    @Bean("DbToDisplay")
    public Job DbToDisplay(JobBuilderFactory jobBuilderFactory,
                           StepBuilderFactory stepBuilderFactory){
        Step step = stepBuilderFactory
                .get("Step to display from Db")
                .<User,DisplayResultDto>chunk(600)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();

        return jobBuilderFactory
                .get("Job to display from db")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }

//    @Bean("DbToDisplayReader")
//    public ItemReader<User> reader() {
//        return new CustomItemReader();
//    }

//    @Bean("DbToDisplayWriter")
//    public FlatFileItemWriter<DisplayResultDto> writer() throws IOException {
//
//        Resource outputResource = new FileSystemResource("src/main/resources/output.xlsx");
//
//        //Create writer instance
//        FlatFileItemWriter<DisplayResultDto> writer = new FlatFileItemWriter<>();
//
//        //Set output file location
//        writer.setResource(outputResource);
//
//        //All job repetitions should "append" to same output file
//        writer.setAppendAllowed(true);
//
//        //Name field values sequence based on object properties
//        writer.setLineAggregator(new DelimitedLineAggregator<DisplayResultDto>() {
//            {
//                setDelimiter(",");
//                setFieldExtractor(new BeanWrapperFieldExtractor<DisplayResultDto>() {
//                    {
//                        setNames(new String[] { "id", "orderID", "region","country","itemType","salesChannel"
//                                ,"orderPriority","Profit","totalProfit" });
//                    }
//                });
//            }
//        });
//        return writer;
//    }
}
