package com.trustrace.SpringBatch.service;

import com.trustrace.SpringBatch.model.DatabaseSequence;
import com.trustrace.SpringBatch.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class SequenceGeneratedService {

    @Autowired
    public MongoOperations mongoOperations;

    public  long generateSequence() {
        DatabaseSequence counter = mongoOperations.findAndModify(query(where("_id").is(User.SEQUENCE_NAME)),
                new Update().inc("seq",1), options().returnNew(true).upsert(true),
                DatabaseSequence.class);
        return !Objects.isNull(counter) ? counter.getSeq() : 1;
    }


}
