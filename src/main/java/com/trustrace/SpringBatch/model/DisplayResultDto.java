package com.trustrace.SpringBatch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DisplayResultDto {

    Long Id;
    Integer orderID;
    String region;
    String  country;
    String itemType;
    String  salesChannel;
    String  orderPriority;
    Float  Profit;
    String  totalProfit;

}
