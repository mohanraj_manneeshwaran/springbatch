package com.trustrace.SpringBatch.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.util.Date;


@Data
@NoArgsConstructor
public class User {

    @Transient
    public static final String SEQUENCE_NAME = "users_sequence";

    @Id
    private long id;

    String region;
    String  country;
    String itemType;
    String  salesChannel;
    String  orderPriority;
    Date orderDate;
    Integer orderID;
    Date shipDate;
    String unitsSold;
    String unitPrice;
    String unitCost;
    String totalRevenue;
    String totalCost;
    String totalProfit;



}
