package com.trustrace.SpringBatch.DbToDisplay;

import com.trustrace.SpringBatch.model.User;
import com.trustrace.SpringBatch.repository.UserRepository;
import org.springframework.batch.item.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component("DbToDisplayReader")
public class CustomItemReader implements ItemReader<User> {

    @Autowired
    UserRepository userRepository;

    private long index=1;


    @Override
    public User read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
           User nextUser = null;

           if(index<= userRepository.count()) {
               nextUser = userRepository.findById((int) index).get();
               index++;
           }else {
             index=1;
           }
           return nextUser;
    }

}


