package com.trustrace.SpringBatch.DbToDisplay;


import com.trustrace.SpringBatch.model.DisplayResultDto;
import com.trustrace.SpringBatch.model.User;
import com.trustrace.SpringBatch.repository.UserRepository;
import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;

@Component("DbToDisplayWriter")
public class Writer implements ItemStreamWriter<DisplayResultDto> {


    public static String SHEET="Users";

    static String[] HEADERs = { "id", "orderID", "region","country","itemType","salesChannel"
                                    ,"orderPriority","Profit","totalProfit" };

    Workbook workbook; FileOutputStream out ;
    Sheet sheet;
    int rowIdx ;

    @SneakyThrows
    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        workbook= new XSSFWorkbook();
        out = new FileOutputStream("src/main/resources/output.xlsx");
        sheet = workbook.createSheet(SHEET);
        rowIdx=1;

        Row headerRow = sheet.createRow(0);
        for (int col = 0; col < HEADERs.length; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(HEADERs[col]);
        }
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
    }

    @Override
    public void write(List<? extends DisplayResultDto> list) throws Exception {

        System.out.println("--------From Writer--------: ");

        try{
            for (DisplayResultDto  user: list) {

                System.out.println("------"+user.getId() +"------RowId-"+rowIdx);

                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(user.getId());
                row.createCell(1).setCellValue(user.getOrderID());
                row.createCell(2).setCellValue(user.getRegion());
                row.createCell(3).setCellValue(user.getCountry());
                row.createCell(4).setCellValue(user.getItemType());
                row.createCell(5).setCellValue(user.getSalesChannel());
                row.createCell(6).setCellValue(user.getOrderPriority());
                row.createCell(7).setCellValue(user.getProfit());
                row.createCell(8).setCellValue(user.getTotalProfit());

            }
            workbook.write(out);
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }
    @SneakyThrows
    @Override
    public void close() throws ItemStreamException {
        workbook.close();
    }
}

//public class Writer implements ItemWriter<DisplayResultDto>{
//
//    @Autowired
//    UserRepository userRepository;
//
//    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
//    static String[] HEADERs = { "id", "orderID", "region","country","itemType","salesChannel"
//                                    ,"orderPriority","Profit","totalProfit" };
//    static String SHEET = "Tutorials";
//
//    String filename = "src/main/resources/output.xlsx";
//
//    Workbook workbook;
//    FileOutputStream out = new FileOutputStream(filename);
//    Sheet sheet;
//    int rowIdx ;
//    private long lastindex;
//    public Writer() throws FileNotFoundException {
//
//        System.out.println("--------------From writer in ------DbToDisplayWriter            ------------");
//
//        workbook= new XSSFWorkbook();
//        sheet = workbook.createSheet(SHEET);
//
//        rowIdx=1;
//        try {
//            lastindex  = userRepository.count();
//
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//
//        }
//        System.out.println("_______________________"+lastindex);
//        Row headerRow = sheet.createRow(0);
//
//        for (int col = 0; col < HEADERs.length; col++) {
//            Cell cell = headerRow.createCell(col);
//            cell.setCellValue(HEADERs[col]);
//        }
//    }
//
//
//
//
//
//}