package com.trustrace.SpringBatch.csvToDb;

import com.trustrace.SpringBatch.model.User;
import com.trustrace.SpringBatch.repository.UserRepository;
import com.trustrace.SpringBatch.service.SequenceGeneratedService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("CsvToDbWriter")
public class Writer  implements ItemWriter<User>{

    @Autowired
    UserRepository userRepository;

    @Autowired
    SequenceGeneratedService sequenceGeneratedService;

    @Override
    public void write(List<? extends User> users) throws Exception {

        for(User user:users){
            user.setId(sequenceGeneratedService.generateSequence());
        }

        userRepository.saveAll(users);
    }
}
